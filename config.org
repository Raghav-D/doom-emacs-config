#+TITLE: Doom Config
#+PROPERTY: header-args :tangle config.el

* Table of Contents :toc:
- [[#general][General]]
- [[#shell][Shell]]
- [[#visual][Visual]]
- [[#keybindings][Keybindings]]
- [[#org-mode][Org Mode]]
- [[#centaur-tabs][Centaur Tabs]]
- [[#enterexit][Enter/Exit]]
- [[#lisp][Lisp]]
- [[#dired][Dired]]
- [[#eww][EWW]]
- [[#elfeed][Elfeed]]
- [[#other][Other]]
- [[#org-roam][Org Roam]]
- [[#doom-dashboard][Doom Dashboard]]
- [[#variables][Variables]]
- [[#evil][Evil]]
- [[#programming][Programming]]
  - [[#clojure][Clojure]]
  - [[#rust][Rust]]
- [[#ivy][Ivy]]
- [[#dashboard][Dashboard]]
- [[#elfeed-1][Elfeed]]
- [[#emms][EMMS]]
- [[#splits][Splits]]

* General
#+begin_src elisp


(setq user-full-name "Raghav Dixit"
      user-mail-address "raghavdv@protonmail.ch")
#+end_src
* Shell
#+begin_src elisp
(setq shell-file-name "/opt/local/bin/fish")
(setq eshell-rc-script "~/.config/doom/eshell/profile"
      eshell-visual-commands '("bash" "fish" "htop" "ssh" "top" "zsh"))
#+end_src
* Visual
#+begin_src elisp
(setq doom-theme 'doom-one)
(set-face-attribute 'default nil
                    :font "IBM Plex Mono 14"
                    :weight 'medium)

(set-face-attribute 'variable-pitch nil
                    :font "IBM Plex Sans 14"
                    :weight 'medium)

(set-face-attribute 'fixed-pitch nil
                    :font "SauceCodePro NF 14"
                    :weight 'medium)

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :weight normal))
(setq global-prettify-symbols-mode t)


(global-visual-line-mode t) ; enables word wrap
(setq kill-whole-line t)
#+end_src
* Keybindings
#+begin_src elisp
(map! :leader
      :desc "Edit doom config"
      "- d c" #'(lambda () (interactive) (find-file "~/.config/doom/config.org"))
      :leader
      :desc "Edit doom packages"
      "- d p" #'(lambda () (interactive) (find-file "~/.config/doom/packages.el"))
      :leader
      :desc "Edit doom init"
      "- d i" #'(lambda () (interactive) (find-file "~/.config/doom/init.el"))
      :leader
      :desc "Edit eshell aliases"
      "- d a" #'(lambda () (interactive) (find-file "~/.config/doom/eshell/aliases"))
      :leader
      :desc "Edit fish config"
      "- f" #'(lambda () (interactive) (find-file "~/.config/fish/config.fish"))
      :leader
      :desc "Edit vifm config"
      "- v c" #'(lambda () (interactive) (find-file "~/.config/vifm/vifmrc"))
      :leader
      :desc "Rust Mode"
      "- r" #'rust-mode
      :leader
      :desc "Autocomplete mode"
      "- a" #'auto-complete-mode
      :leader
      :desc "Company Mode"
      "- c" #'company-mode
      :leader
      :desc "Toggle Neotree"
      "o n" #'neotree-toggle
      :leader
      :desc "Open Emacs Keybindings Cheat Sheet"
      "- e" #'(lambda () (interactive) (find-file "~/org-mode/emacs-vs-evil.org")))


(map! :leader
      "_" #'dmenu)
#+end_src
* Org Mode
#+begin_src elisp

(after! org

  (require 'org-superstar)
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
  ;; (use-package org-bullets
    ;; :ensure t
        ;; :init
        ;; (add-hook 'org-mode-hook (lambda ()
        ;; ;; (org-bullets-mode 1))))
        ;;


  (defun my/org-mode/load-prettify-symbols ()
    (interactive)
    (setq prettify-symbols-alist
          (mapcan (lambda (x) (list x (cons (upcase (car x)) (cdr x))))
        '(("#+begin_src" . ?)
            ("#+end_src" . ?)
            ("#+begin_example" . ?)
            ("#+end_example" . ?)
            ("#+DATE:" . ?⏱)
            ("#+AUTHOR:" . ?✏)
            ("[ ]" .  ?☐)
            ("[X]" . ?☑ )
            ("[-]" . ?❍ )
            ("lambda" . ?λ)
            ("#+header:" . ?)
            ("#+name:" . ?﮸)
            ("#+results:" . ?)
            ("#+call:" . ?)
            (":properties:" . ?)
            (":logbook:" . ?))))
    (prettify-symbols-mode 1))

  (setq org-directory "~/org-mode/"
        org-agenda-files '("~/org-mode")
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-log-done 'time
        org-journal-dir "~/Org/journal/"
        org-journal-date-format "%B %d, %Y (%A) "
        org-journal-file-format "%Y-%m-%d.org"
        org-hide-emphasis-markeiirs t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "HOMEWORK(h)"
             "CLASS(C)"
             "PROJ(p)"
             "TEST(T)"
             "EVENT(e)"
             "EXAM(E)"
             "|"
             "DONE(d)"
             "CANCELLED(c)"
             "ANYTIME(a)"

        ; (require 'org-bullets)
        ; (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
             ))))
#+end_src

#+RESULTS:
| sequence | TODO(t) | HOMEWORK(h) | CLASS(C) | PROJ(p) | TEST(T) | EVENT(e) | Exam(E) |   |   | DONE(d) | CANCELLED(c) | ANYTIME(a) |

* Centaur Tabs
#+begin_src elisp
;; (setq centaur-tabs-set-bar 'over
;;       centaur-tabs-set-icons t
;;       centaur-tabs-gray-out-icons 'buffer
;;       centaur-tabs-height 24
;;       centaur-tabs-set-modified-marker t
;;       centaur-tabs-style "bar"
;;       centaur-tabs-modified-marker "•")
;; (map! :leader
;;       :desc "Toggle tabs globally" "t c" #'centaur-tabs-mode
;;       :desc "Toggle tabs local display" "t C" #'centaur-tabs-local-mode)
;; (evil-define-key 'normal centaur-tabs-mode-map (kbd "g <right>") 'centaur-tabs-forward        ; default Doom binding is 'g t'
;;                                                (kbd "g <left>")  'centaur-tabs-backward       ; default Doom binding is 'g T'
;;                                                (kbd "g <down>")  'centaur-tabs-forward-group
;;
;                                                (kbd "g <up>")    'centaur-tabs-backward-group)
#+end_src

#+RESULTS:

* Enter/Exit
#+begin_src elisp
(add-to-list 'initial-frame-alist '(fullscreen . maximized))
(setq confirm-kill-emacs nil)
(setq auto-save-default t
      make-backup-files t)
#+end_src

#+RESULTS:
: t

* Lisp
#+begin_src elisp
;; (load (expand-file-name "~/.quicklisp/slime-helper.el"))
;; (setq inferior-lisp-program "sbcl")
#+end_src

#+RESULTS:

* Dired
#+begin_src elisp


(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))
;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-chmod
  (kbd "O") 'dired-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))
(use-package peep-dired
  :ensure t
  :defer t ; don't access `dired-mode-map' until `peep-dired' is loaded
  :bind (:map dired-mode-map
         ("P" . peep-dired)))
#+end_src
#+RESULTS:
: peep-dired

* EWW
#+begin_src elisp
(map! :leader
      :desc "EWW web browser"
      "e w" #'eww
      :leader
      :desc "EWW reload page"
      "e R" #'eww-reload
      :leader
      :desc "Search web for text between BEG/END"
      "s w" #'eww-search-words)

#+end_src

#+RESULTS:
: /opt/local/bin/fish

* Elfeed
#+begin_src elisp


; BEGIN ELFEED
(require 'elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(defvar elfeed-feeds-alist
                    '(("https://odysee.com/$/rss/@DistroTube:2" Distrotube Odysee)
                     ("https://www.reddit.com/r/linux.rss" Linux Reddit)
                      ("https://www.reddit.com/r/commandline.rss" Linux Reddit)
                      ("http://opensource.com/feed" Linux Opensource)
                      ("https://distrowatch.com/news/dwd.xml" Linux Distrowatch)
                       ("https://reddit.com/r/emacs.rss")))
;; (setq browse-url-browser-function 'eww-browse-url)
#+end_src

#+RESULTS:
: eww-browse-url

* Other
#+begin_src elisp


(require 'auto-complete)
(global-auto-complete-mode t)
(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
#+end_src

#+RESULTS:
: ((gnu . https://elpa.gnu.org/packages/) (melpa . https://melpa.org/packages/) (org . https://orgmode.org/elpa/) (melpa-stable . http://stable.melpa.org/packages/))

* Org Roam
#+begin_src elisp
;; (org-roam-db-autosync-mode)
;; (add-to-list 'display-buffer-alist
;;              '("\\*org-roam\\*"
;;                (display-buffer-in-direction)
;;                (direction . right)
;;                (window-width . 0.33)
;;                (window-height . fit-window-to-buffer)))
;; (use-package org-roam
;;   :ensure t
;;   :init
;;   (setq org-roam-v2-ack t)
;;   :custom
;;   (org-roam-directory "~/org-roam")
;;   :bind (("C-c n l" . org-roam-buffer-toggle)
;;          ("C-c n f" . org-roam-node-field)
;;          ("C-c n i" . org-roam-node-insert))
;;   :config
;;   (org-roam-setup))
#+end_src

#+RESULTS:

* Doom Dashboard
#+begin_src elisp

;(defun my/dashboard-banner ()
  ;"""Set a dashboard banner including information on package initialization
;   time and garbage collections."""
  ;(setq dashboard-banner-logo-title
;        (format "Emacs ready in %.2f seconds with %d garbage collections."
   ;             (float-time (time-subtract after-init-time before-init-time)) gcs-done)))
;
;; (use-package dashboard
  ;; :init
  ;; (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  ;; (add-hook 'dashboard-mode-hook 'my/dashboard-banner)
  ;; :config
  ;; (setq dashboard-startup-banner 'logo)
  ;; (dashboard-setup-startup-hook))
  #+end_src

  #+RESULTS:

* Variables
#+begin_src elisp
(setq gdscript-godot-executable t)
(setq org-support-shift-select t)
(setq org-clock-sound "~/Downloads/99595927.mp3")
#+end_src

#+RESULTS:
: ~/Downloads/99595927.mp3
* Evil
#+begin_src elisp
;; Get hybrid mode working
;(use-package evil
;  :custom
;  evil-disable-insert-state-bindings t)
(defalias 'evil-insert-state 'evil-emacs-state)
(define-key evil-emacs-state-map (kbd "<escape>") 'evil-normal-state)
#+end_src
* Programming
** Clojure
#+begin_src elisp
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

(setq package-selected-packages '(clojure-mode lsp-mode cider lsp-treemacs flycheck company))

(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))

(add-hook 'clojure-mode-hook 'lsp)
(add-hook 'clojurescript-mode-hook 'lsp)
(add-hook 'clojurec-mode-hook 'lsp)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      treemacs-space-between-root-nodes nil
      company-minimum-prefix-length 1
      lsp-lens-enable t
      lsp-signature-auto-activate nil)
      ; lsp-enable-indentation nil ; uncomment to use cider indentation instead of lsp
      ; lsp-enable-completion-at-point nil ; uncomment to use cider completion instead of lsp
      (setq lsp-enable-completion-at-point nil) ; use cider completion

#+end_src
** Rust
#+begin_src elisp
;; (use-package rustic
;;   :ensure

;;               ("M-?" . lsp-find-references)
;;               ("C-c C-c l" . flycheck-list-errors)
;;               ("C-c C-c a" . lsp-execute-code-action)
;;               ("C-c C-c r" . lsp-rename)
;;               ("C-c C-c q" . lsp-workspace-restart)
;;               ("C-c C-c Q" . lsp-workspace-shutdown)
;;               ("C-c C-c s" . lsp-rust-analyzer-status))
;;   :config
;;   ;; uncomment for less flashiness
;;   ;; (setq lsp-eldoc-hook nil)
;;   ;; (setq lsp-enable-symbol-highlighting nil)
;;   ;; (setq lsp-signature-auto-activate nil)

;;   ;; comment to disable rustfmt on save
;;   (setq rustic-format-on-save t)
;;   (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

;; (defun rk/rustic-mode-hook ()
;;   ;; so that run C-c C-c C-r works without having to confirm, but don't try to
;;   ;; save rust buffers that are not file visiting. Once
;;   ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
;;   ;; no longer be necessary.
;;   (when buffer-file-name
;;     (setq-local buffer-save-without-query t)))
;; ;
                                        ; (use-package company
;;   ;; ... see above ...
;;   (:map company-mode-map
;; 	("<tab>". tab-indent-or-complete)
;; 	("TAB". tab-indent-or-complete)))

;; (defun company-yasnippet-or-completion ()
;;   (interactive)
;;   (or (do-yas-expand)
;;       (company-complete-common)))

;; (defun check-expansion ()
;;   (save-excursion
;;     (if (looking-at "\\_>") t
;;       (backward-char 1)
;;       (if (looking-at "\\.") t
;;         (backward-char 1)
;;         (if (looking-at "::") t nil)))))

;; (defun do-yas-expand ()
;;   (let ((yas/fallback-behavior 'return-nil))
;;     (yas/expand)))

;; (defun tab-indent-or-complete ()
;;   (interactive)
;;   (if (minibufferp)
;;       (minibuffer-complete)
;;     (if (or (not yas/minor-mode)
;;             (null (do-yas-expand)))
;;         (if (check-expansion)
;;             (company-complete-common)
;;           (indent-for-tab-command)))))
#+end_src
* Ivy
#+begin_src elisp
(setq ivy-posframe-display-functions-alist
      '((swiper                     . ivy-posframe-display-at-point)
        (complete-symbol            . ivy-posframe-display-at-point)
        (counsel-M-x                . ivy-display-function-fallback)
        (counsel-esh-history        . ivy-posframe-display-at-window-center)
        (counsel-describe-function  . ivy-display-function-fallback)
        (counsel-describe-variable  . ivy-display-function-fallback)
        (counsel-find-file          . ivy-display-function-fallback)
        (counsel-recentf            . ivy-display-function-fallback)
        (counsel-register           . ivy-posframe-display-at-frame-bottom-window-center)
        (nil                        . ivy-posframe-display))
      ivy-posframe-height-alist
      '((swiper . 40)
        (t . 10)))
(ivy-posframe-mode 1) ; 1 enables posframe-mode, 0 disables it.
#+end_src
* Dashboard
#+begin_src elisp
(use-package dashboard
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Welcome to Doom Emacs!")
  (setq dashboard-startup-banner 'logo)) ; Makes logo Default banner
  ;; (setq dashboard-startup-banner "~/.config/doom/logo.jpg")  ; use custom image as banner
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents . 5)
                          (agenda . 5 )
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))

  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book")))
#+end_src
* Elfeed
#+begin_src elisp
(use-package! elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(add-hook 'elfeed-show-mode-hook 'visual-line-mode)
(evil-define-key 'normal elfeed-show-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(defvar elfeed-feeds-alist)
                    '(("https://www.reddit.com/r/linux.rss" reddit linux)
                     ("https://www.reddit.com/r/commandline.rss" reddit commandline)
                     ("https://www.reddit.com/r/ClashRoyale.rss" reddit clashroyale)
                     ("https://www.reddit.com/r/ProgrammerHumor.rss" reddit programming humor)
                     ("https://www.reddit.com/r/emacs.rss" reddit emacs)
                     ("https://odysee.com/$/rss/@DistroTube:2" odysee distrotube)
                     ("https://www.gamingonlinux.com/article_rss.php" gaming linux)
                     ("https://opensource.com/feed" opensource linux)
                     ("https://itsfoss.com/feed/" itsfoss linux)
                     ("https://www.computerworld.com/index.rss" computerworld linux)
                     ("https://www.networkworld.com/category/linux/index.rss" networkworld linux)
                     ("https://www.techrepublic.com/rssfeeds/topic/open-source/" techrepublic linux)
                     ("https://distrowatch.com/news/dwd.xml" distrowatch linux))
#+end_src
* EMMS
#+begin_src elisp
(emms-all)
(emms-default-players)
(emms-mode-line 1)
(emms-playing-time 1)
(setq emms-source-file-default-directory "~/Music/Non-Classical/70s-80s/"
      emms-playlist-buffer-name "*Music*"
      emms-info-asynchronously t
      emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)
(map! :leader
      (:prefix ("a" . "EMMS audio player")
       :desc "Go to emms playlist" "a" #'emms-playlist-mode-go
       :desc "Emms pause track" "x" #'emms-pause
       :desc "Emms stop track" "s" #'emms-stop
       :desc "Emms play previous track" "p" #'emms-previous
       :desc "Emms play next track" "n" #'emms-next))
#+end_src
* Splits
#+begin_src elisp
(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 50 t))

(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window"
      "b c" #'clone-indirect-buffer-other-window)
#+end_src
